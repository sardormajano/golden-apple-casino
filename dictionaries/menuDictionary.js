var menuDictionary = {
    "golden-apple-casino": {
        "rus": 'Казино "Алтын Алма"',
        "eng": '"Altyn Alma" casino',
        "kaz": '"Алтын Алма" казиносi'
    },
    "management-accounting" : {
        "rus": "Управленческий учет",
        "eng": "Management accounting",
        "kaz": "Басқарушылық есеп"
    },
    "sub-management-accounting" : {
        "rus": "Отчет о себестоимости услуг в казино за определенный период",
        "eng": "The report on the cost of services to the casino for a certain period",
        "kaz": "Белгілі бір кезең үшін казино қызметтердің құнын туралы есеп"
    },
    "scenario-analysis" : {
        "rus": "Сценарный анализ",
        "eng": "Scenario analysis",
        "kaz": "Сценарий талдау"
    },
    "sub-scenario-analysis" : {
        "rus": "Инструмент определения возможностей по сокращению расходов и увеличению доходов",
        "eng": "Tool to identify opportunities to reduce costs and increase revenue",
        "kaz": "Шығындарды азайту және пайданы арттыру үшін мүмкіндіктерін анықтау үшін құралы"
    },
    "data-collection" : {
        "rus": "Сбор данных",
        "eng": "Data collection",
        "kaz": "Деректер жинау"
    },
    "sub-data-collection" : {
        "rus": "Форма для ввода отчета расходов и доходов за прошлый период",
        "eng": "The form for entering the account of costs and revenues for the previous period",
        "kaz": "Алдыңғы кезең үшін шығындар мен кірістерді есепке енгізу үшін нысаны"
    },
    "research-institute" : {
        "rus": '2016 © ТОО "Научно-исследовательский институт экономики и информатизации транспорта, телекоммуникаций"',
        "eng": '2016 © LLP "Scientific-Research Institute of Economy and Informatization of transport, telecommunications"',
        "kaz": '2016 © ЖШС «көлік Экономика және ақпараттандыру ғылыми-зерттеу институты, телекоммуникациялар»'
    },
};
