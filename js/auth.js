function main()
{
    var submitButton = qs("#submit-button"),
        login = qs("#login"),
        password = qs("#password");

    submitButton.addEventListener("click", function(event){
        event.preventDefault();
        if(!login.value)
        {
            addClass(login, "warning");
            setTimeout(function(){
                removeClass(login, "warning");
            }, 1000);
        }
        if(!password.value)
        {
            addClass(password, "warning");
            setTimeout(function(){
                removeClass(password, "warning");
            }, 1000);
        }
    });
}

ready(main);
