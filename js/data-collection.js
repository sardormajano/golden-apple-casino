function main()
{
    var dropDownList = qs(".drop-down-list"),
        rightSide = qs(".right-side"),
        formTitle = rightSide.qs(".form-title"),
        fieldSet = rightSide.qs("fieldset"),
        dateSpan = qs(".left-side .date"),
        date = new Date(),
        year = date.getFullYear(),
        month = date.getMonth() < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1),
        day = date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate();

    dateSpan.innerText = year + "-" + month + "-" + day;

    makeDropDownable(dropDownList, dataCollection);

    var functionalLi = qsa(".functional-li");

    addEventListeners(functionalLi, "click", function(event){
        formTitle.innerText = event.target.getAttribute("data-legend");
        var currentMeters = forms[parseInt(event.target.getAttribute("data-form-number"))];

        fieldSet.innerHTML = "";

        for(var i = 0, len = currentMeters.length; i < len; i++)
        {
            var label = document.createElement("label"),
                input = document.createElement("input"),
                span = document.createElement("span");

            label.innerText = currentMeters[i];
            label.setAttribute("for", "input-" + i);
            input.setAttribute("name", "input-" + i);
            input.setAttribute("type", "text");
            input.setAttribute("placeholder", "введите " + currentMeters[i]);
            span.setAttribute("class", "form-line");


            span.appendChild(label);
            span.appendChild(input);

            fieldSet.appendChild(span);
        }

        var submit = document.createElement("input");

        submit.setAttribute("type", "submit");
        submit.setAttribute("value", "сохранить");
        submit.setAttribute("class", "submit-button");
        fieldSet.appendChild(document.createElement("label"));
        fieldSet.appendChild(submit);
    });
}

ready(main);
