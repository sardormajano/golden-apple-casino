var qs = document.querySelector.bind(document),
    qsa = document.querySelectorAll.bind(document);

Element.prototype.qs = Element.prototype.querySelector,
Element.prototype.qsa = Element.prototype.querySelectorAll;

function ready(fn)
{
    if(document.readyState !== "loading")
    {
        fn();
    }
    else
    {
        document.addEventListener("DOMContentLoaded", fn);
    }
}

function hide(element)
{
    if(element instanceof NodeList)
    {
        for(var i = 0, len = element.length; i < len; i++)
        {
            element[i].style.display = "none";
        }
    }
    else
    {
        element.style.display = "none";
    }
}

function show(element)
{
    if(element instanceof NodeList)
    {
        for(var i = 0, len = element.length; i < len; i++)
        {
            element[i].style.display = "";
        }
    }
    else
    {
        element.style.display = "";
    }
}

function toggle(element)
{
    if(element instanceof NodeList)
    {
        for(var i = 0, len = element.length; i < len; i++)
        {
            element[i].style.display === "none" ? element[i].style.display = "" :
                                            element[i].style.display = "none";
        }
    }
    else
    {
        element.style.display === "none" ? element.style.display = "" :
                                     element.style.display = "none";
    }
}

function addClass(element, className)
{
    if(element instanceof NodeList)
    {
        Array.prototype.forEach.call(element, function(currElement){
            currElement.classList.add(className);
        });
    }
    else
    {
        element.classList.add(className);
    }
};

function removeClass(element, className)
{
    if(element instanceof NodeList)
    {
        Array.prototype.forEach.call(element, function(currElement){
            currElement.classList.remove(className);
        });
    }
    else
    {
        element.classList.remove(className);
    }
}

function toggleClass(element, className)
{
    if(element.classList.contains(className))
    {
        element.classList.remove(className);
    }
    else
    {
        element.classList.add(className);
    }
}

function addEventListeners(nodeList, eventName, listener)
{
    Array.prototype.forEach.call(nodeList, function(node){
        node.addEventListener(eventName, listener);
    });
}
