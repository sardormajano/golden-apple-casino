function liClickListener(event)
{
    var dropDownList = event.target.nextElementSibling;

    if(dropDownList && dropDownList.tagName.toLowerCase() === "ul")
        toggleClass(dropDownList, "hidden");
}

function fillTheTable(unorderedList, data)
{
    for(var i = 0, len = data.length; i < len; i++)
    {
        if(typeof data[i] === "string")
        {
            var li = document.createElement("li");

            li.innerText = data[i];
            li.setAttribute("onclick", "liClickListener(event)");
            unorderedList.appendChild(li);
        }
        else if(Array.isArray(data[i]))
        {
            var ul = document.createElement("ul");

            ul.setAttribute("class", "hidden");
            fillTheTable(ul, data[i]);
            unorderedList.appendChild(ul);
        }
        else
        {
            var li = document.createElement("li");

            li.innerText = data[i]["innerText"];

            for(key in data[i]["attributes"])
            {
                li.setAttribute(key, data[i]["attributes"][key])
            }

            unorderedList.appendChild(li);
        }
    }
}

function makeDropDownable(unorderedList, data)
{
    unorderedList.innerHTML = "";
    fillTheTable(unorderedList, data);
}
