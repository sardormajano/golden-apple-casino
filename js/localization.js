function localize(dictionary)
{
    var langButtons = qsa(".lang-button");

    removeClass(langButtons, "underline");
    addClass(qsa("." + activeLanguage + "-button"), "underline");

    for(var key in dictionary)
    {
        var element = qs("." + key);

        element.innerText = dictionary[key][activeLanguage];
    }
}

ready(localize.bind(window, menuDictionary));
