var pieChartCanvas, pieChartCanvasParent, pieChartCtx,
    pieChart;

function getFormattedDate(date)
{
    var currDate = new Date(date)

    year = currDate.getFullYear(),
    month = currDate.getMonth() < 10 ? "0" + (currDate.getMonth() + 1) : (currDate.getMonth() + 1),
    day = currDate.getDate() < 10 ? ("0" + currDate.getDate()) : currDate.getDate();

    return year + "-" + month + "-" + day
}

function main()
{
    var dropDownList = qs(".drop-down-list"),
        dateButton = qs(".date-button"),
        dateInput = qs(".date-input"),
        picker = new Pikaday({
            field: dateButton,
            onSelect: function(){
                var date = getFormattedDate(this._d);
                dateInput.setAttribute("value", date);
            }
        }),
        tableType = qs(".table-type");

    makeDropDownable(dropDownList, managementAccounting[0]);

    tableType.addEventListener("change", function(event){
        makeDropDownable(dropDownList, managementAccounting[parseInt(event.target.value)]);
    });
}

ready(main);
