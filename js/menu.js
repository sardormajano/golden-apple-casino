var activeLanguage;

function main(){
    var menuButtons = qsa(".menu-button"),
        langButtons = qsa(".lang-button");

    activeLanguage = "kaz";

    Array.prototype.forEach.call(langButtons, function(button){
        button.addEventListener("click", function(event){
            activeLanguage = event.target.innerText.toLowerCase();
            localize(menuDictionary);
        });
    });
};

ready(main);
